#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 16:53:16 2019

@author: vaishnavi
"""
def extract_scores(file_name):
    file = open(file_name, "r")
    contents = file.readlines()
    scores = []
    for line in contents:
        scores.append(line.split())
    return scores

def gen_valid_pair(score1, score2):
    n = len(score1)
    marks_diff = [int(score1[a]) - int(score2[a]) for a in range(1,n)]
    if all(i >= 0 for i in marks_diff):
        return score1[0],score2[0]
    if all(i <= 0 for i in marks_diff):
        return score2[0],score1[0]
    return ()

def gen_pairs(file_name):
    scores = extract_scores(file_name)
    valid_pairs = []
    for i in range(0,len(scores) - 1):
        for j in range(i+1, len(scores)):
            valid_pair = gen_valid_pair(scores[i], scores[j])
            if len(valid_pair) != 0:
                valid_pairs.append((valid_pair[0], valid_pair[1]))
    return valid_pairs

def rem_trans_pairs(valid_pairs):
    for i in range(0,len(valid_pairs) - 1):
        for j in range(i+1, len(valid_pairs)):
            print(valid_pairs[i][0])
            if valid_pairs[i][0] == valid_pairs[j][1]:
                valid_pairs.remove((valid_pairs[j][0], valid_pairs[i][1]))
            if valid_pairs[i][1] == valid_pairs[j][0]:
                valid_pairs.remove((valid_pairs[i][0], valid_pairs[j][1]))
    print(valid_pairs)         
                
file_name = 'ranking.txt'
valid_pairs = gen_pairs(file_name)
print(valid_pairs)
#rem_trans_pairs(valid_pairs)